import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hive_database_demo/data_model.dart';
import 'package:flutter_hive_database_demo/screens/login_page.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
// import 'package:flutter_hive_database_demo/screens/login_page.dart';
// import 'package:flutter_hive_database_demo/utils/fire_auth.dart';

const String dataBoxName = "data";

class MyHomePage extends StatefulWidget {
  final User user;

  const MyHomePage({required this.user});

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

enum DataFilter { ALL, COMPLETED, PROGRESS }

class _MyHomePageState extends State<MyHomePage> {
  late Box<DataModel> dataBox;
  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  DataFilter filter = DataFilter.ALL;

  // ignore: unused_field
  bool _isSigningOut = false;
  // ignore: unused_field
  late User _currentUser;

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    _currentUser = widget.user;
    super.initState();
    dataBox = Hive.box<DataModel>(dataBoxName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(200),
          child: AppBar(
            backgroundColor: Colors.amber,
            leading: IconButton(
                icon: Icon(
                  Icons.keyboard_arrow_left,
                  color: Colors.grey[900],
                ),
                onPressed: () {
                  Navigator.pop(context);
                }),
            actions: <Widget>[
              PopupMenuButton<String>(
                icon: Icon(
                  Icons.filter_list,
                  color: Colors.grey[900],
                ),
                color: Colors.grey[900],
                onSelected: (value) {
                  if (value.compareTo("All") == 0) {
                    setState(() {
                      filter = DataFilter.ALL;
                    });
                  } else if (value.compareTo("Compeleted") == 0) {
                    setState(() {
                      filter = DataFilter.COMPLETED;
                    });
                  } else {
                    setState(() {
                      filter = DataFilter.PROGRESS;
                    });
                  }
                },
                itemBuilder: (BuildContext context) {
                  return ["All", "Compeleted", "Progress"].map((option) {
                    return PopupMenuItem(
                      value: option,
                      child: Text(option),
                    );
                  }).toList();
                },
              ),
              IconButton(
                onPressed: () async {
                  setState(() {
                    _isSigningOut = true;
                  });
                  await FirebaseAuth.instance.signOut();
                  setState(() {
                    _isSigningOut = false;
                  });
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) => LoginPage(),
                    ),
                  );
                },
                icon: Icon(Icons.logout),
                color: Colors.grey[900],
              ),
            ],
            flexibleSpace: Positioned(
              child: Container(
                padding: EdgeInsets.only(top: 47),
                child: Column(
                  children: [
                    Text(
                      "BucketList",
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey[900]),
                    ),
                    Row(
                      children: [
                        SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Hi, ",
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[900]),
                                ),
                                Text(
                                  '${_currentUser.displayName}',
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[900]),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(150, 0, 0, 10),
                          child: Icon(
                            Icons.person,
                            size: 100,
                            color: Colors.grey[900],
                          ),
                        )
                      ],
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[900],
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30))),
                      alignment: Alignment.center,
                      height: 41,
                      width: 300,
                      // padding: EdgeInsets.only(top: 20),
                      child: Text(
                        "Create Your BucketList",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w300,
                            color: Colors.grey[300]),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 8,
            ),
            ValueListenableBuilder(
              valueListenable: dataBox.listenable(),
              builder: (context, Box<DataModel> items, _) {
                List<int> keys;

                if (filter == DataFilter.ALL) {
                  keys = items.keys.cast<int>().toList();
                } else if (filter == DataFilter.COMPLETED) {
                  keys = items.keys
                      .cast<int>()
                      .where((key) => items.get(key)!.complete)
                      .toList();
                } else {
                  keys = items.keys
                      .cast<int>()
                      .where((key) => !items.get(key)!.complete)
                      .toList();
                }

                return ListView.separated(
                  separatorBuilder: (_, index) => Divider(),
                  itemCount: keys.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (_, index) {
                    final int key = keys[index];
                    final DataModel? data = items.get(key);
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.grey[200],
                      child: ListTile(
                          title: Text(
                            data!.title,
                            style: TextStyle(fontSize: 20, color: Colors.black),
                          ),
                          subtitle: Text(data.description,
                              style: TextStyle(
                                  fontSize: 17, color: Colors.black38)),
                          leading: CircleAvatar(
                            backgroundColor: Colors.grey[900],
                            child: Icon(
                              Icons.pending_actions,
                              color: Colors.grey[100],
                            ),
                          ),
                          trailing: IconButton(
                              onPressed: () {
                                dataBox.deleteAt(index);
                              },
                              icon: Icon(Icons.delete))
                          // Icon(
                          //   Icons.check,
                          //   color: data.complete
                          //       ? Colors.deepPurpleAccent
                          //       : Colors.red,
                          // ),
                          // onTap: () {
                          //   showDialog(
                          //       builder: (context) => Dialog(
                          //           backgroundColor: Colors.amber,
                          //           shape: RoundedRectangleBorder(
                          //             borderRadius: BorderRadius.circular(15.0),
                          //           ),
                          //           child: Container(
                          //             padding: EdgeInsets.all(16),
                          //             child: Column(
                          //               mainAxisSize: MainAxisSize.min,
                          //               children: <Widget>[
                          //                 // ignore: deprecated_member_use
                          //                 FlatButton(
                          //                   shape: RoundedRectangleBorder(
                          //                     borderRadius:
                          //                         BorderRadius.circular(10.0),
                          //                   ),
                          //                   color: Colors.grey[900],
                          //                   child: Text(
                          //                     "Mark as complete",
                          //                     style: TextStyle(
                          //                         color: Colors.grey[200]),
                          //                   ),
                          //                   onPressed: () {
                          //                     DataModel mData = DataModel(
                          //                         title: data.title,
                          //                         description: data.description,
                          //                         complete: true);
                          //                     dataBox.put(key, mData);
                          //                     Navigator.pop(context);
                          //                   },
                          //                 )
                          //               ],
                          //             ),
                          //           )),
                          //       context: context);
                          // },
                          ),
                    );
                  },
                );
              },
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.grey[900],
        onPressed: () {
          showDialog(
              builder: (context) => Dialog(
                  backgroundColor: Colors.grey[900],
                  child: Container(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        TextField(
                          decoration: InputDecoration(hintText: "BucketList"),
                          controller: titleController,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Padding(
                            padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: "Description",
                              ),
                              controller: descriptionController,
                            )),
                        SizedBox(
                          height: 8,
                        ),
                        // ignore: deprecated_member_use
                        FlatButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: Colors.green,
                          child: Text(
                            "Add Data",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            final String title = titleController.text;
                            final String description =
                                descriptionController.text;
                            titleController.clear();
                            descriptionController.clear();
                            DataModel data = DataModel(
                                title: title,
                                description: description,
                                complete: false);
                            dataBox.add(data);
                            Navigator.pop(context);
                          },
                        )
                      ],
                    ),
                  )),
              context: context);
        },
        child: Icon(
          Icons.add,
          color: Colors.grey[200],
        ),
      ),
    );
  }
}
